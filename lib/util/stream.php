<?php

class CY_Util_Stream
{
	static function default_write($stream, $body, $options)
	{
		$length = strlen($body);

		do
		{
			if(($rv = fwrite($stream, $body, $length)) == false)
			{
				$id = (int)$stream;
				cy_log(CYE_ERROR, $options['server'].' default_write error');
				return array('errno' => CYE_NET_ERROR);
			}

			$length -= $rv;
			if($length)
			{
				$body = substr($body, $rv);
			}

		}while($length);

		return array('errno' => 0);
	}

	static function default_read($stream, $options)
	{
		static $length = 4096;
		$data = '';

		stream_set_blocking($stream, 1);
		do
		{
			if(($buf = fread($stream, $length)) === false)
			{
				$id = (int)$stream;
				cy_log(CYE_ERROR, $options['server'].' default_read error');
				return array('errno' => CYE_NET_ERROR);
			}

			$data .= $buf;
		}
		while($buf !== NULL && strlen($buf) === $length);
		return array('errno' => 0, 'data' => $data);
	}
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
?>
