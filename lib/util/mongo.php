<?php

class CY_Util_Mongo
{
	protected $config;
	protected $mongo;

	protected $c;	

	function __construct($config = NULL)
	{
		$this->config = empty($config) ? $_ENV['config']['mongo_ming'] : $config;
		$this->connect();
	}

	function connect()
	{
		if(!$this->mongo)
		{
			$i = array_rand($this->config);
			$c = $this->config[$i];
			$this->c = $c;

			try
			{
				$opt = [];
				$opt['connectTimeoutMS'] = $_ENV['config']['timeout']['mongo_connect'] * 1000;
				$opt['socketTimeoutMS' ] = $_ENV['config']['timeout']['mongo_read'   ] * 1000;
				$opt['connect']          = false;
				$this->mongo = new MongoClient($c['uri'], $opt); 
			}
			catch(Exception $e)
			{
				$this->mongo = NULL;
				cy_log(CYE_ERROR, $e->getMessage());
			}
		}
	}

	function mSet($data, $options = [])
	{
		if(empty($opt['update']))
		{
			return $this->mInsert($data, $options);
		}

		$array = [];
		foreach($data as $row)
		{
			$dt = $this->save($row, $options);
			if($dt['errno'] !== 0)
			{
				foreach($dt['data'] as $k => $v)
				{
					$array[$k] = $v;	
				}
			}
		}

		return cy_dt(0, $array);
	}

	function __call($method, $args)
	{
		$data = [];
		$call = ['mGet' => 'find', 'save' => 'save', 'mInsert' => 'batchInsert', 'delete' => 'remove', 'update' => 'update'];
		$getid= ['mGet' => false , 'save' => 'save', 'mInsert' => true         , 'delete' => false   , 'update' => true];
		if(empty($call[$method]))
		{
			return cy_dt(-1, 'unkown method.');
		}

		$t1 = microtime(true);

		$table  = array_shift($args);
		$dbname = $this->c['database'];

//		try
		{
			$db   = $this->mongo->$dbname;

			$c10n = $table == 'file' ? $db->getGridFS() : $db->$table;
			$back = call_user_func_array([$c10n, $call[$method]], $args);
			if(is_object($back))
			{
				$back = iterator_to_array($back);
			}

			else if($getid[$method] && $back)
			{
				$list = array();
				foreach($args[0] as $k => $v)
				{
					$list[$k] = (string)$v['_id'];
				}

				$back = $list;
			}

			$data = cy_dt(0, $back);
		}
/*
		catch(MongoCursorException $e)
		{
			$data = cy_dt(1);
		}
		catch(MongoCursorTimeoutException $e)
		{
			cy_log(CYE_ERROR, "mongo-$method ".substr($e->getMessage(), 0, 512));
			cy_log(CYE_ERROR, "mongo-$method close=%d, reconnect=%d", $this->mongo->close(), $this->mongo->connect);

			$data = cy_dt(CYE_SYSTEM_ERROR);
		}
*/

		// end process.
		$cost = (microtime(true) - $t1)*1000000;
		cy_stat('mongo-'.$method, $cost);
		return $data;
	}

}


?>
