<?php
/*
 * Http based multi fetch model.
 *
 * By: jianyu@carext.com
 */

class CY_Util_Curl
{
	protected $mh;
	protected $timeout = 4000;

	protected $handles = array();
	protected $hosts   = array();

	protected $count   = 0;

	function __construct()
	{
		$this->mh = curl_multi_init();
	}

	function __destruct()
	{
		curl_multi_close($this->mh);
	}

	function size()
	{
		return $this->count;
	}

	function mix($ch, $c)
	{
		$d = curl_getinfo($ch);
		$d['errno'] = curl_errno($ch);
		if($d['errno'] != CURLE_OK)
		{
			$d['error'] = curl_error($ch);
			//cy_ctl_fail($this->hosts[$key], $_SERVER['REQUEST_TIME']);

			return $d;
		}

		if(isset($d['download_content_length']) &&
				$d['download_content_length'] != -1 &&
				$d['download_content_length'] != $d['size_download'])
		{
			$d['error'] = 'not finish, maybe timeout is too small.';
			cy_log(CYE_WARNING, '%s not finish, time cost %fs', $d['url'], $d['total_time']);
		}

		$contents = curl_multi_getcontent($ch);
		$options  = $c->options();
		if(isset($options['include']))
		{
			$header   = substr($contents, 0, $d['header_size']);
			$contents = substr($contents,    $d['header_size']);
			$d['headers'] = http_parse_headers($header);
		}

		$d['data']  = $c->outputs($contents);
		$d['class'] = get_class($c);
		$d['proxy'] = isset($options['proxy']) ? $options['proxy'] : '';

		curl_multi_remove_handle($this->mh, $ch);
		curl_close($ch);

		unset($this->handles[(int)$ch]);
		$this->count--;

		return $d;
	}

	/**
	 * add
	 *
	 * @param $key  string  unique key for each request.
	 * @param $c    array   config object.
	 *
	 * @return true | false.
	 */
	function add($key, $c)
	{
		$options = $c->inputs([]);
		if(empty($options['url']))
		{
			return false;
		}

		$url   = $options['url'];
		$parts = parse_url($url);
		$host  = $parts['host'].(isset($parts['port']) ? ':'.$parts['port'] : '');
		$this->hosts[$key] = $host;
		/*
		   if(!cy_ctl_check($host, $_SERVER['REQUEST_TIME']))
		   {
		   return false;
		   }
		 */

		/* get options */
		$timeout = isset($options['timeout']) ? $options['timeout'] : $this->timeout;
		$method  = isset($options['method' ]) ? $options['method' ] : 'get';
		if(isset($options['data']))
		{
			if(strncasecmp($method, 'get', 3) === 0)
			{
				$query = http_build_query($data);
				if(strpos($url, '?') === false)
				{
					$url .= '?'.$query;
				}
				else if($url[strlen($url)-1] == '&')
				{
					$url .= $query;
				}
				else
				{
					$url .= '&'.$query;
				}
			}
		}

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER        , !empty($options['include']));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_ENCODING      , 'gzip');
		curl_setopt($ch, CURLOPT_NOSIGNAL      , true);
		if(defined('CURLOPT_TIMEOUT_MS'))
		{
			curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
		}
		else
		{
			curl_setopt($ch, CURLOPT_TIMEOUT, ceil($timeout/1000));
		}

		if(strncasecmp($method, 'post', 4) === 0)
		{
			curl_setopt($ch, CURLOPT_POST, 1);
			if(isset($options['data']))
			{
				curl_setopt($ch, CURLOPT_POSTFIELDS, $options['data']);
			}

			/* Disable Expect: header  */
			isset($options['header']) || $options['header'] = [];
			$options['header'][] = 'Expect: ';
		}

		if(isset($options['header']))
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, $options['header']);
		}


		if(isset($options['proxy']))
		{
			curl_setopt($ch, CURLOPT_PROXY, $options['proxy']);
		}
		elseif(isset($_ENV['config']['http_proxy']))
		{
			$which = array_rand($_ENV['config']['http_proxy']);
			$proxy = $_ENV['config']['http_proxy'][$which];
			curl_setopt($ch, CURLOPT_PROXY, $proxy[0].':'.$proxy[1]);
		}

		curl_multi_add_handle($this->mh, $ch);

		$this->handles[(int)$ch] = array($ch, $c, $key);

		$this->count++;
		return true;
	}

	function recv($callback = NULL, $timeout = 0.1)
	{
		$active = 0; /* A reference to a flag to tell whether the cURLs are still running  */

		$t1 = $t2 = microtime(true);
		if($this->count < 1)
		{
			return cy_dt(1, 'empty handle.');
		}

		$errno = curl_multi_exec($this->mh, $active);
		if($errno != CURLM_OK)
		{
			return cy_dt($errno, curl_strerror($errno));
		}

		while($this->count === $active && ($t2 - $t1) < $timeout)
		{
			$ready = cy_curl_multi_select($this->mh, $timeout, 0x01|0x4);
			if($ready < 0)
			{
				return cy_dt(-1, 'curl_multi_select error');
			}

			$errno = curl_multi_exec($this->mh, $active);
			if($errno != CURLM_OK)
			{
				return cy_dt($errno, curl_strerror($errno));
			}

			$t2 = microtime(true);
		}

		$data = [];
		while(($r = curl_multi_info_read($this->mh, $msgs_in_queue)))
		{
			$ch = $r['handle'];
			list(, $c, $key) = $this->handles[(int)$ch];
			$data[$key]=$mix = $this->mix($ch, $c);
			if($callback)
			{
				call_user_func($callback, $key, $mix);
			}
		}

		return cy_dt(OK, $data);
	}

	function get()
	{
		$active = null;
		$t1 = microtime(true);

		do
		{
			$mrc = curl_multi_exec($this->mh, $active);
		}
		while($mrc == CURLM_CALL_MULTI_PERFORM);

		while($active && $mrc == CURLM_OK)
		{
			PHP_VERSION_ID < 50214 ? usleep(5000) : curl_multi_select($this->mh);
			do
			{
				$mrc = curl_multi_exec($this->mh, $active);
			}
			while($mrc == CURLM_CALL_MULTI_PERFORM);
		}

		$stat = array();
		$data = array();
		foreach($this->handles as $id => list($ch, $c, $key))
		{
			$data[$key] = $this->mix($ch, $c);
		}

		$cost = (microtime(true) - $t1)*1000000;
		cy_stat('Curl-get', $cost, array('c' => implode(',', $stat)));
		return array('errno' => 0, 'data' => $data);
	}

	function fetch($url, $method = 'GET', $headers = array(), $options = array())
	{
		$getinfo = !empty($options['getinfo']);
		unset($options['getinfo']);

		$c = new CY_Driver_Http_Default($url, $method, $headers, $options);
		$this->add(0, $c);
		$dt = $this->get();
		if($dt['errno'] !== 0 || $dt['data'][0]['errno'] !== 0)
		{
			return NULL;
		}

		return $getinfo ? $dt['data'][0] : $dt['data'][0]['data'];
	}
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
?>
