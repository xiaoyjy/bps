<?php

function cy_regex_extract($string, $regex, $which = 0)
{
	if(preg_match($regex, $string, $m))
	{
		return $m[$which];
	}

	return '';
}

function cy_remove_html_attr($html, $attrs = [])
{
	$pattern = [];
	foreach($attrs as $attr)
	{
		$pattern[] = '/'.$attr.'\s*=\s*(["\']).*?\1\s*/';
	}

	return preg_replace($pattern, '', $html);
}

function cy_is_w($char)
{
	$n = ord($char);
	return (64 < $n && $n < 91) || (96 < $n && $n < 123) || (47 < $n && $n < 58);
}

/**
 * NOTICE:
 *  tag space shoud be remove before invoke this function.
 *  eg: </ a > shoud be replace to </a>
 *
 * @author:
 *	 Jianyu<xiaoyjy@gmail.com>
 *
 * Example:
 *
 * $html: 
 *
 * 
 */
function cy_split_by_tag1($html, $tags = [])
{
	$length = strlen($html);

	/* 防止出Warings，又减少计算量的track做法 */
	for($i = 0; $i < $length; $i++)
	{
begin:
		/* NOTICE: $i just at '|' */
		/**
		 *  Find out:
		 *	   xxxx|<foo>
		 *	   xxxx|<foo ...>
		 *     or  xxxx|</foo>
		 *
		 *  but ignore:
		 *         xxxx|<html> end of xxxx will be ignored.
		 */
		$p = strpos($html, '<', $i);

		/***
		 * Last html tag
		 *
		 * may be:
		 *     </html>|\r\n
		 * 
		 */
		if($p === false)
		{
			/* Skip tag will make non empty $text. */
			if(!empty($text))
			{
				yield $i => trim($text);
			}

			break;
		}

		/* xxxx|<foo> or xxxx|</foo>, restore 'xxxx' as new line. */
		if(!empty($text))
		{
			/**
			 * Right now $text may be the first part of:
			 *  <foo .... />| or
			 *  <foo...>..</foo>| or
			 *  <foo xxxxxxx>|....</foo>
			 *  </foo>|
			 */

			/* Skip tag will make non empty $text. */
			$text .= "\n";
			$text .= trim(substr($html, $i, $p - $i));

			yield $i => trim($text);
			$text  = '';
		}
		else
		{
			yield $i => trim(substr($html, $i, $p - $i));
		}

		$i = strpos($html, '>', $p);
		if(!$i)
		{
			cy_log(CYE_WARNING, 'invalid html content "<" without ">" at the end.');
			break;
		}

		$w = ($html[$p+1] !== '/') ? 1 : 2;

		/**
		 * Then:
		 *  $i <foo|>xxxx or </foo|>xxxx or <foo ...|> or </html|>
		 *  $p |<foo>xxxx or |</foo>xxxx or |<foo ...> or |</html>
		 */

		/**
		 * Get next html tag name
		 */
		$y = $p;
		while(cy_is_w($html[++$y]));

		/**
		 * Right now
		 *  $i: <foo|>xxxx or </foo|>xxxx or <foo ...|> or </html|>
		 *  $p: |<foo>xxxx or |</foo>xxxx or <foo ...|> or |</html>
		 *  $y: <foo|>xxxx or </foo|>xxxx or <foo| ...> or </html|>
		 *  $p + $w:
		 *      <|foo>xxxx or </|foo>xxxx or <|foo ...> or </|html>
		 *
		 *  so $t == 'foo'
		 */
		$t = strtolower(substr($html, $p + $w, $y - $p - $w));

		/* if need skip */
		if(!empty($t) && in_array($t, $tags))
		{
			/**
			 * <p> is a very speical tag
			 * most of web editors support <p>, contents in <p> should not be separated.
			 */
			if($t === 'p')
			{
				if($w === 2)
				{
					cy_log(CYE_WARNING, "found </p> without <p>, skip it.");

					$i = $p + 4 /* strlen('</p>') */;
					goto begin; 
				}

				$y = $k = $p;
				/* $y, $k, $p:
				 *   |<p>...</p>                   or
				 *   |<p ...>...</p>               or
				 *   |<p> .. <p>.. </p> </p>       or 
				 *
				 * Invalid:
				 *   |<p> ..<p> </p> not enongh '</p>'
				 */
				do
				{
					/* Invalid <p> tag, just skip it. */
					if(($x = stripos($html, '</p>', $k)) === false)
					{
						cy_log(CYE_WARNING, "content at $p found unclosed <p> tag");

						$i = $p + 3 /* 3 = sizeof('<p>') */;

						/* Right new $i at <p>| .. <p> .. </p> */
						goto begin;
					}

					$k = $x + 4/*strlen('</p>')*/;
					/**
					 * $x: <p> .. <p>.. |</p> </p>
					 $ $k: <p> .. <p>.. </p>| </p>
					 * $p: |<p> .. <p>.. </p> </p>
					 */

					$z = substr($html, $y, $k - $y);

					/* Make sure $z have't <p...> or <p> */
					do
					{
						$j = stripos($z, '<p' , 2/* strlen(<p>)==3' */);
					}
					/**
					 * But not <pxxx>, such as <pre>, <param>, etc. 
					 * if match that, just pass through
					 *
					 * $j:
					 *  ....|<param>.. 
					 *
					 * if <param> ...
					 *   z = substr('.....<param>...<p>...</p>', );
					 *   z = aram>...<p>...</p>
					 */
					while(cy_is_w($z[$j+2]) && ($z = substr($z, $j + 2/* strlen(<p)==2 */)));

					/**
					 * so, $j = position of <p..> | <p> or false.
					 */
					$y += (int)$j; 
				}
				while($j);

				/**
				 * finally:
				 * 
				 * $p: |<p> .. <p>.. </p> </p>
				 * $k: <p> .. <p>.. </p> </p>|
				 */
			}

			else
			{
				/*
				 * $p: |<foo...> or |</foo> 
				 * $k: <foo...>| or </foo>|
				 */ 
				$k = strpos($html, '>', $p) + 1;
			}

			/* save the skipped html into $text */
			$text = substr($html, $p, $k - $p);
			$i    = $k - 1;

		} /* skip tags end . */
	}
}

function cy_html_repair($html, $encoding = 'UTF8')
{
	$config = array
		(
		 'clean'         => true,
		 'output-xml'    => true,
		 'output-xhtml'  => true,
		 'wrap'          => 200
		);


	$t = new tidy();
	$t->parseString($html, $config, $encoding);
	$t->cleanRepair();

	// fix html
	return $t->html();
}

function cy_html_charset($string)
{
	if(preg_match('/charset(\s*?)=(\s*?)["\']?([\w-]+)"/i', $string, $m))
	{
		$encode = strtoupper($m[3]);
		if($encode == 'UTF8')
		{
			$encode = 'UTF-8';
		}

		return $encode;
	}

	$orders = ["ISO-8859-1", 'UTF-8', 'GB18030', 'BIG-5'];
	return mb_detect_encoding($string, $orders);
}

function cy_standard_date($str)
{
	$cn=array('二十','三十',
			'十一','十二','十三','十四','十五','十六','十七','十八','十九',
			'一','二','三','四','五','六','七','八','九','十',
			'零', '○','年', '月', '日','/','–', '时', '分', '秒');

	$num=array('2','3',
			'11','12','13','14','15','16','17','18','19',
			'1','2','3','4','5','6','7','8','9','10',
			'0', '0','-', '-', '', '-','-', ':', ':', '');

	return rtrim(str_replace($cn, $num, $str), ':');
}

function cy_iconv_recursive($ar, $from = 'GB18030', $to = 'UTF-8//IGNORE')
{
	if(empty($ar))
	{
		return $ar;
	}

	if(is_string($ar))
	{
		return iconv($from, $to, $ar);
	}

	if(!is_array($ar))
	{
		return $ar;
	}

	$new_ar = array();
	foreach($ar as $k => $v)
	{
		$new_ar[$k] = cy_iconv_recursive($v);
	}

	return $new_ar;
}

function cy_split_html_title($title)
{
	$combine = function($nodes, $c)
	{
		$array = [];
		$count = count($nodes);
		for($i = 0; $i < $count; $i++)
		{
			$end = $count - $i;
			$len = $i + 1;
			for($j = 0; $j < $end; $j++)
			{
				$tmp = [];
				for($k = 0; $k < $len; $k++)
				{
					$tmp[] = $nodes[$j + $k];
				}

				$array[] = implode($c, $tmp);
			}
		}

		return $array;
	};

	$array = [];
	foreach(['|', '_', '-', '/',' '] as $c)
	{
		$array[] = $combine(array_map('trim', explode($c, $title)), $c); 
	}

	/* 去除括号 */
	$array[][] = trim(preg_replace('/\(.*/', '', $title));
	return $array;
}


?>
