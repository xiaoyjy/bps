<?php

/***/
$_ENV['config']['log'] = array
(
 'path'  => CY_HOME.'/log',
 'name'  => 'place',
 'level' => 8,
 'fflush' => false 
);

$_ENV['config']['stat_max_line'] = 256;
$_ENV['config']['xhprof_enable'] = 0;

$_ENV['config']['timeout'] = array
(
 'redis_connect' => 0.2,
 'redis_read' => 0.4,

 'mysql_connect' => 1,
 'mysql_read' => 1,

 'http_connect' => 0.2,
 'http_read' => 0.5,

 'net_default' => 0.6,
);

?>
