<?php

define('CY_BPS_VERSION'    , '0.3.1');

define("CY_SRV_MAX_REQUEST", 25000);
define("CY_SRV_CLI_TIMEOUT", 500000); // 500ms

$_ENV['server'] = ['http' => []];
$_ENV['server']['http']['listen'] = 'tcp://0.0.0.0:8088';
$_ENV['server']['http']['srv_num'] = 4;

$_SERVER['HTTP_HOST'] = 'bps.example.com';

$_ENV['config']['dead_lock_force_exit'] = true;

?>
